# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.

def list_laporan(request):
	return render(request, 'daftar_laporan.html')
