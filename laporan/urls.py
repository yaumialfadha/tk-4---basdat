from django.conf.urls import url
from django.urls import path
from .views import list_laporan
from django.conf.urls.static import static
from django.conf import settings

#url for app

app_name = 'laporan'
urlpatterns = [
    path('', list_laporan, name='listlaporan')
]