# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect

from django.shortcuts import render

# Create your views here.
def riwayat_sewa(request):
	return render(request, 'transaksi-sewa.html')

def riwayat_fee(request):
	return render(request, 'transaksi-fee.html')

def mitra_bank(request):
	return render(request, 'transaksi-mitra.html')