from django.urls import path
from django.conf.urls import url
from . import views
from django.conf.urls.static import static
from django.conf import settings

app_name = 'transaksi'
urlpatterns = [
    path('riwayat-sewa/', views.riwayat_sewa, name='riwayat-sewa'),
    path('riwayat-fee/', views.riwayat_fee, name='riwayat-fee'),
    path('mitra-bank/', views.mitra_bank, name='mitra-bank')
]  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
