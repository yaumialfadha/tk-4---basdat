# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.

def daftar_penyewa(request):
	return render(request, 'daftar_penyewa.html')
def daftar_pemilik(request):
	return render(request, 'daftar_pemilik.html')
def daftar_staff(request):
	return render(request, 'daftar_staff.html')

