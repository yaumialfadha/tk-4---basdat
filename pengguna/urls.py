from django.urls import path
from django.conf.urls import url
from . import views
from django.conf.urls.static import static
from django.conf import settings

app_name = 'pengguna'
urlpatterns = [
    path('daftar_penyewa/', views.daftar_penyewa, name='daftar-penyewa'),
    path('daftar-pemilik/', views.daftar_pemilik, name='daftar-pemilik'),
    path('daftar-staff/', views.daftar_staff, name='daftar-staff')
]  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
