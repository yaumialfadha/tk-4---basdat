"""TK4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import transaksi.urls as transaksi
from transaksi import views
import login.urls as login
from login import views
import laporan.urls as laporan
from laporan import views
import pengguna.urls as pengguna
from pengguna import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^transaksi/', include(('transaksi.urls', 'transaksi'), namespace='transaksi')),
    url(r'^', include(('login.urls', 'login'), namespace='login')),
    url(r'^laporan/', include(('laporan.urls', 'laporan'), namespace='laporan')), 
    url(r'^pengguna/', include(('pengguna.urls', 'pengguna'), namespace='pengguna')),
    # url(r'^daftar_penyewa/', include(('pengguna.urls', 'daftar_penyewa'), namespace='pengguna')),   
    # url(r'^daftar_pemilik/', include(('pengguna.urls', 'daftar_pemilik'), namespace='pengguna')),   
    # url(r'^daftar_staff/', include(('pengguna.urls', 'daftar_staff'), namespace='daftar_staff')),   
    url(r'^lukisan/', include(('lukisan.urls', 'lukisan'), namespace='lukisan')),   
]
