from django.urls import path
from django.conf.urls import url
from .views import index
from django.conf.urls.static import static
from django.conf import settings

app_name = 'login'
urlpatterns = [
    path('', index, name='index')
]