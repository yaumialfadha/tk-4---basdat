from django.conf.urls import url
from django.urls import path
from .views import *
from django.conf.urls.static import static
from django.conf import settings


app_name = 'lukisan'
urlpatterns = [
    path('', lukisan, name='lukisan')
]